import React, { Component } from "react";
import { connect } from "react-redux";
import { toggleGrid } from "actions";
import "assets/css/components/image-grid/components/grid-controls/components/GridToggle.scss";

const stateProperties = (state, props) => {
  let storeProps = {};
  if (state[props.id]) {
    storeProps = { visibility: state[props.id].visibilty };
  }

  return storeProps;
};

const actionFunctions = dispatch => {
  return {
    setVisibilty: (id, e) => {
      dispatch(toggleGrid(id, e.target.checked));
    }
  };
};

export class GridToggle extends Component {
  render() {
    const { id, setVisibilty, visibility } = this.props;

    return (
      <div className="grid-toggle">
        <label>
          Enable Grid
          <input
            type="checkbox"
            onChange={e => setVisibilty(id, e)}
            defaultChecked={true}
            checked={visibility}
          />
        </label>
      </div>
    );
  }
}

const wrappedComponent = connect(
  stateProperties,
  actionFunctions
)(GridToggle);

export default wrappedComponent;
