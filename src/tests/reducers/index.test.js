import React from "react";
import reducer from "reducers/index.js";

describe("reducers/index.js: ", () => {
  it("Should return an empty object when called with a non existent type", () => {
    const action = { type: "TEST" };
    expect(reducer({}, action)).toEqual({});
  });

  describe("ADD_GRID", () => {
    it("Should return an empty object when called with a no id", () => {
      const action = { type: "ADD_GRID" };
      expect(reducer({}, action)).toEqual({});
    });

    it("Should create a 'grid' state object with the action id as a key", () => {
      const action = { type: "ADD_GRID", id: "test" };
      expect(reducer({}, action)[action.id]).toEqual({
        columns: 0,
        visibility: true
      });
    });
  });

  describe("SET_COLUMNS", () => {
    let state = {};

    beforeEach(() => {
      state = {
        test: {
          columns: 0,
          visibility: true
        }
      };
    });

    it("Should not 'mutate' state when called with no arguments", () => {
      const action = { type: "SET_COLUMNS" };
      expect(reducer(state, action)).toEqual(state);
    });

    it("Should not 'mutate' state when called with a non existent id", () => {
      const action = { type: "SET_COLUMNS", id: "NOPE" };
      expect(reducer(state, action)).toEqual(state);
    });

    it("Should not 'mutate' state when called without a 'action.value'", () => {
      const action = { type: "SET_COLUMNS", id: "NOPE" };
      expect(reducer(state, action)).toEqual(state);
    });

    it("Should 'mutate' state when called with correct id and a value", () => {
      const action = { type: "SET_COLUMNS", id: "test", value: 1 };
      expect(reducer(state, action)).toEqual({
        test: {
          columns: 1,
          visibility: true
        }
      });
    });
  });

  describe("TOGGLE_GRID", () => {
    let state = {};

    beforeEach(() => {
      state = {
        test: {
          columns: 0,
          visibility: true
        }
      };
    });

    it("Should not 'mutate' state when called with no arguments", () => {
      const action = { type: "TOGGLE_GRID" };
      expect(reducer(state, action)).toEqual(state);
    });

    it("Should not 'mutate' state when called with a non existent id", () => {
      const action = { type: "TOGGLE_GRID", id: "NOPE" };
      expect(reducer(state, action)).toEqual(state);
    });

    it("Should not 'mutate' state when called without a 'action.boolean'", () => {
      const action = { type: "TOGGLE_GRID", id: "NOPE" };
      expect(reducer(state, action)).toEqual(state);
    });

    it("Should 'mutate' state when called with correct id and a boolean", () => {
      const action = { type: "TOGGLE_GRID", id: "test", boolean: false };
      expect(reducer(state, action)).toEqual({
        test: {
          columns: 0,
          visibility: false
        }
      });
    });
  });
});
