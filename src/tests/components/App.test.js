import React from "react";
import App from "components/App";
import ImageGrid from "components/image-grid/ImageGrid";
import { renderComponent } from "tests/helpers.js";

describe("<App/>: ", () => {
  it("Should render with an ImageGrid subcomponent", () => {
    const result = renderComponent(<App />);
    expect(result.props.children).toEqual(<ImageGrid id="grid1" />);
  });
});
