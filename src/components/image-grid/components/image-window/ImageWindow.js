import React, { Component } from "react";
import svg from "assets/svg/image.svg";
import GridOverlay from "components/image-grid/components/image-window/components/GridOverlay";
import "assets/css/components/image-grid/components/image-window/ImageWindow.css";

export default class ImageWindow extends Component {
  render() {
    const { id } = this.props;

    return (
      <div className="image-window">
        <GridOverlay id={id} />
        <img src={svg} />
      </div>
    );
  }
}
