import React, { Component } from "react";
import { connect } from "react-redux";
import GridControls from "components/image-grid/components/grid-controls/GridControls";
import ImageWindow from "components/image-grid/components/image-window/ImageWindow";
import { addGrid } from "actions";

const actionFunctions = dispatch => {
  return {
    addGrid: id => {
      dispatch(addGrid(id));
    }
  };
};

export class ImageGrid extends Component {
  constructor(props) {
    super(props);

    props.addGrid(props.id);
  }

  render() {
    const { columns, id } = this.props;

    return (
      <div className="image-grid">
        <GridControls id={id} />
        <ImageWindow id={id} />
      </div>
    );
  }
}

export default connect(
  null,
  actionFunctions
)(ImageGrid);
