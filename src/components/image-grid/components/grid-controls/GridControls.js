import React, { Component } from "react";
import GridToggle from "components/image-grid/components/grid-controls/components/ColumnInput";
import ColumnInput from "components/image-grid/components/grid-controls/components/GridToggle";
import "assets/css/components/image-grid/components/grid-controls/GridControls.scss";

export class GridControls extends Component {
  render() {
    const { id } = this.props;

    return (
      <div className="grid-controls">
        <ColumnInput id={id} />
        <GridToggle id={id} />
      </div>
    );
  }
}

export default GridControls;
