import React from "react";
import { ImageGrid } from "components/image-grid/ImageGrid";
import GridControls from "components/image-grid/components/grid-controls/GridControls";
import ImageWindow from "components/image-grid/components/image-window/ImageWindow";
import { renderComponent } from "tests/helpers.js";

describe("<ImageGrid/>: ", () => {
  it("Should render with the correct number of children", () => {
    const result = renderComponent(
      <ImageGrid id="test" addGrid={() => null} />
    );

    expect(result.props.children.length).toEqual(2);
  });

  it("Should pass 'id' property to child components", () => {
    const result = renderComponent(
      <ImageGrid id="test" addGrid={() => null} />
    );

    expect(JSON.stringify(result.props.children)).toEqual(
      JSON.stringify([<GridControls id="test" />, <ImageWindow id="test" />])
    );
  });

  it("Should call the add grid dispatcher with the id property value", () => {
    let idProp = "";
    const result = renderComponent(
      <ImageGrid
        id="test"
        addGrid={value => {
          idProp = value;
        }}
      />
    );

    expect(idProp).toEqual("test");
  });
});
