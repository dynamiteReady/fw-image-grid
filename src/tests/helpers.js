import ShallowRenderer from "react-test-renderer/shallow";

export const renderComponent = component => {
  const renderer = new ShallowRenderer();
  renderer.render(component);
  return renderer.getRenderOutput();
};
