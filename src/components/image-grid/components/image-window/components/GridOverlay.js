import React, { Component } from "react";
import { connect } from "react-redux";
import "assets/css/components/image-grid/components/image-window/components/GridOverlay.scss";

const stateProperties = (state, props) => {
  let storeProps = {};
  if (state[props.id]) {
    storeProps = {
      columns: Number(state[props.id].columns) + 1,
      visibility: state[props.id].visibility
    };
  }

  return storeProps;
};

export class GridOverlay extends Component {
  generateGridLines(orientation, value = 0) {
    let lines = [];
    for (let i = 0; i < Number(value); i++)
      lines.push(<div key={`${orientation}-${i}`} />);
    return lines;
  }

  render() {
    const { columns, visibility } = this.props;

    return (
      <div className={`grid-overlay${visibility ? "" : " hide"}`}>
        <div className="vertical">
          {this.generateGridLines("vertical", columns)}
        </div>
        <div className="horizontal">
          {this.generateGridLines("horizontal", columns)}
        </div>
      </div>
    );
  }
}

const wrappedComponent = connect(
  stateProperties,
  null
)(GridOverlay);

export default wrappedComponent;
