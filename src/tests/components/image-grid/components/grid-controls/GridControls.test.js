import React from "react";
import GridControls from "components/image-grid/components/grid-controls/GridControls";
import ColumnInput from "components/image-grid/components/grid-controls/components/ColumnInput";
import GridToggle from "components/image-grid/components/grid-controls/components/GridToggle";
import { renderComponent } from "tests/helpers.js";

describe("<GridControls/>: ", () => {
  it("Should render with the correct number of children", () => {
    const result = renderComponent(<GridControls />);

    expect(result.props.children.length).toEqual(2);
  });

  it("Should pass 'id' property to child components", () => {
    const result = renderComponent(<GridControls id="test" />);

    expect(JSON.stringify(result.props.children)).toEqual(
      JSON.stringify([<ColumnInput id="test" />, <GridToggle id="test" />])
    );
  });
});
