export const addGrid = id => ({
  type: "ADD_GRID",
  id
});

export const setColumns = (id, value) => ({
  type: "SET_COLUMNS",
  id,
  value
});

export const toggleGrid = (id, boolean) => ({
  type: "TOGGLE_GRID",
  id,
  boolean
});
