import React from "react";
import { GridOverlay } from "components/image-grid/components/image-window/components/GridOverlay";
import { renderComponent } from "tests/helpers.js";

describe("<GridOverlay/>: ", () => {
  it("Should render with the correct number of children by default", () => {
    const result = renderComponent(<GridOverlay />);
    expect(result.props.children.length).toEqual(2);
  });

  describe("GridOverlay.generateGridLines: ", () => {
    it("Should return the requested number of div elements", () => {
      const gridOverlay = new GridOverlay();
      expect(gridOverlay.generateGridLines("TEST", 3).length).toEqual(3);
    });

    it("Should add a unique key to each generated element", () => {
      const gridOverlay = new GridOverlay();
      expect(JSON.stringify(gridOverlay.generateGridLines("TEST", 1))).toEqual(
        JSON.stringify([
          {
            type: "div",
            key: "TEST-0",
            ref: null,
            props: {},
            _owner: null,
            _store: {}
          }
        ])
      );
    });
  });

  it("Should render with the correct number of subelements into '.vertical' and '.horizontal'", () => {
    const result = renderComponent(<GridOverlay columns={3} />);
    expect(result.props.children[0].props.children.length).toEqual(3);
    expect(result.props.children[1].props.children.length).toEqual(3);
  });

  it("Should hide the component when visibility is set to false", () => {
    const result = renderComponent(<GridOverlay visibility={false} />);
    expect(result.props.className).toEqual("grid-overlay hide");
  });

  it("Should show the component when visibility is set to true", () => {
    const result = renderComponent(<GridOverlay visibility={true} />);
    expect(result.props.className).toEqual("grid-overlay");
  });
});
