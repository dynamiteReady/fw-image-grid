export default (state = {}, action) => {
  let updatedState = { ...state };

  switch (action.type) {
    case "ADD_GRID":
      // When a new 'ImageGrid' component is added to the DOM
      // The constructor should call this method, and set a unique
      // ID. The ID will then be used to create a state object on
      // the global store for the 'ImageGrid' component.
      if (action.id !== undefined) {
        updatedState[action.id] = {
          columns: 0,
          visibility: true
        };
      }

      return updatedState;
    case "SET_COLUMNS":
      // Set number of columns and rows per instance.
      if (updatedState[action.id] && action.value)
        updatedState[action.id].columns = action.value;

      return updatedState;
    case "TOGGLE_GRID":
      // Set visibility of a given instance.
      if (updatedState[action.id] && action.boolean !== undefined)
        updatedState[action.id].visibility = action.boolean;

      return updatedState;
    default:
      return state;
  }
};
