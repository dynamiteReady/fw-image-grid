import React from "react";
import { ColumnInput } from "components/image-grid/components/grid-controls/components/ColumnInput";
import { renderComponent } from "tests/helpers.js";

describe("<ColumnInput/>: ", () => {
  it("Should render with an event handler attached to the input field", () => {
    const noop = (id, e) => {};
    const result = renderComponent(
      <ColumnInput id="test" setNumberOfColumns={noop} />
    );

    expect(JSON.stringify(result.props.children)).toEqual(
      JSON.stringify(
        <label>
          Columns<input type="number" min="2" max="20" onChange={noop} />
        </label>
      )
    );
  });
});
