import React, { Component } from "react";
import ImageGrid from "components/image-grid/ImageGrid";
import "assets/css/components/App.css";

export default class App extends Component {
  render() {
    return (
      <div className="app">
        {/* "id" must be unique for each instance you add. i.e */}
        <ImageGrid id="grid1" />
        {/* <ImageGrid id="grid2" /> */}
        {/* <ImageGrid id="grid3" /> */}
        {/* etc... */}
      </div>
    );
  }
}
