import React from "react";
import { GridToggle } from "components/image-grid/components/grid-controls/components/GridToggle";
import { renderComponent } from "tests/helpers.js";

describe("<GridToggle/>: ", () => {
  it("Should render with an event handler attached to the input field", () => {
    const noop = (id, e) => {};
    const result = renderComponent(
      <GridToggle id="test" visibility={true} setVisibility={noop} />
    );

    expect(JSON.stringify(result.props.children)).toEqual(
      JSON.stringify(
        <label>
          Enable Grid
          <input
            type="checkbox"
            onChange={noop}
            defaultChecked={true}
            checked={true}
          />
        </label>
      )
    );
  });
});
