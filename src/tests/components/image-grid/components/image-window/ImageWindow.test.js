import React from "react";
import ImageWindow from "components/image-grid/components/image-window/ImageWindow";
import GridOverlay from "components/image-grid/components/image-window/components/GridOverlay";
import { renderComponent } from "tests/helpers.js";
import svg from "assets/svg/image.svg";

describe("<ImageWindow/>: ", () => {
  it("Should render with an <img> subelement", () => {
    const result = renderComponent(<ImageWindow id="test" />);
    expect(result.props.children).toEqual([
      <GridOverlay id="test" />,
      <img src={svg} />
    ]);
  });
});
