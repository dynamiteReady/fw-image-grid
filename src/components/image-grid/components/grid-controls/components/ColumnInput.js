import React, { Component } from "react";
import { connect } from "react-redux";
import { setColumns } from "actions";
import "assets/css/components/image-grid/components/grid-controls/components/ColumnInput.scss";

const actionFunctions = dispatch => {
  return {
    setNumberOfColumns: (id, e) => {
      dispatch(setColumns(id, e.target.value));
    }
  };
};

export class ColumnInput extends Component {
  render() {
    const { id, setNumberOfColumns } = this.props;

    return (
      <div className="column-input">
        <label>
          Columns
          <input
            type="number"
            min="2"
            max="20"
            onChange={e => setNumberOfColumns(id, e)}
          />
        </label>
      </div>
    );
  }
}

const wrappedComponent = connect(
  null,
  actionFunctions
)(ColumnInput);

export default wrappedComponent;
