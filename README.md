# fw-image-grid

## Notes:
- Use the common React-Create-App scripts to run the project.
-- npm install
-- npm test
-- npm start
- Added React-Test-Renderer. 
- Added Sass-Loader and Node-Sass. 
- Added Redux.
- npm 'ejected' React-Create-App config to edit Webpage config.

## Known bugs:
- IE (11) doesn't respect min max attributes on '<input type=number>'.
- No event handler tests (could explore other test utilities in the future).
- No tests for action methods.